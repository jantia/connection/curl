<?php

/**
 * Jantia
 *
 * @package        Jantia/Connection/Curl
 * @license        Proprietary
 */
declare( strict_types=1 );

//
namespace Jantia\Connection\Curl\Exception;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class BadMethodCallException extends \BadMethodCallException implements ExceptionInterface {

}
